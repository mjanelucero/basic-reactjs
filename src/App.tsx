// import { useState } from 'react'
// import reactLogo from './assets/react.svg'
// import viteLogo from '/vite.svg'
import './App.css'
import { RouterProvider, createBrowserRouter } from 'react-router-dom';

import Home from "./pages/Home"
import Login from "./pages/Home/Login";
import Register from "./pages/Home/Register";
import User from './pages/Home/Users';
import Posts from './pages/Home/Post';
import ResponsiveAppBar from './pages/Home/Navbar';

import { protectedRoute, isLoggedIn } from './utils/protectedRoute';

const router = createBrowserRouter([
{
  path: "/Login",
  element: <Login />,
  loader: isLoggedIn
},
{
  path: "/",
  element: <Home />,
  loader: protectedRoute
},
{
  path: "/Register",
  element: <Register />,
  loader: isLoggedIn
},
{
  path: "/User",
  element: <User />,
  loader: protectedRoute
},
{
  path: "/Post",
  element: <Posts />,
  loader: protectedRoute
},
{
  path: "/Navbar",
  element: <ResponsiveAppBar />,
  loader: protectedRoute
},
{
  path: "*",
  element: <h1>Error 400 Page Not Found</h1>
},
])

function App(){
  return (
    <RouterProvider router={router}/>
  )
}

export default App;
