export interface IProp {}

export interface IState {
    userList: User[];
}

interface User {
    _id: string;
    name: string;
    username: string;
    password: string,
    email: string;
    role: 'ADMIN' | 'TEAMLEAD' | 'INTERN'
}