import React, { useState, useEffect } from "react";
import axios from "axios";
import { IProp, IState } from "./type";
import { useNavigate } from 'react-router-dom';
import Sheet from '@mui/joy/Sheet';
import CssBaseline from '@mui/joy/CssBaseline';
import Input from '@mui/joy/Input';
import { Button } from '@mui/material'


function User(_props: IProp) {
    const [userList, setUserList] = useState<IState['userList']>([]);
    const [showPass, setShowPass] = useState<boolean>(false);

    const [state, setState] = useState({
        name: "",
        email: "",
        password: "",
        role: "",
        nameError: "",
        roleError: "",
        emailError: "",
        passwordError: "",
    });

    useEffect(() => {
        handleRequestUsers();
    }, []);


    const handleRequestUsers = async () => {
        try {
            const response = await axios.get('http://localhost:5173/api/user');
            setUserList(response.data);
        } catch (error) {
            alert(error);
        }
    };

    const nameHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setState({ ...state, name: value });
    };

    const emailHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setState({ ...state, email: value });
    };

    const roleHandler = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const { value } = event.target;
        setState({ ...state, role: value });
    };

    const passHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setState({ ...state, password: value });
    };

    const errorValidation = () => {
        let nameError = "";
        let passwordError = "";
        let roleError = "";
        let emailError = "";

        if (!state.name.trim()) {
            nameError = "Please enter a name";
        }

        if (!state.password) {
            passwordError = "Please enter a password";
        }

        if (!state.role) {
            roleError = "Please select a role";
        }

        if (!state.email) {
            emailError = "Please enter your email";
        }

        setState({
            ...state,
            nameError, passwordError, roleError, emailError
        });

        return !(nameError || passwordError || roleError || emailError);
    };

    const handleSubmit = async (e: any) => {
        e.preventDefault();
        const isValid = errorValidation();

        if (isValid) {
            const id = Date.now().toString();
            const userData = { ...state, id };

            try {
                const response = await axios.post("http://localhost:5173/api/user", userData);

                console.log(response);
                alert("Successfully submitted");
            }
            catch (err) {
                console.log("Error: ", err);
                alert("Failed to update user.");
            }
            setState({
                name: "",
                role: "",
                email: "",
                password: "",
                emailError: "",
                nameError: "",
                roleError: "",
                passwordError: ""
            });
            
        }
    };

    const navigate = useNavigate();

    const handleButtonHome = () => {
        navigate("/");
    };

    const handleButtonLogout = () => {
        localStorage.removeItem('Token');
        navigate("/Login")
    };

    const handleButtonPost = () => {
        navigate("/Post")
    };

    return (
        <div className='center'> 
        <div>
        <button className='btn' onClick={handleButtonHome}>Home</button>
        <button className='btn' onClick={handleButtonPost}>Post</button>
        <button className='btn' onClick={handleButtonLogout}>Logout</button> 
          <br />
        </div>
          <main>
            <CssBaseline />
              <Sheet
                sx={{
                  backgroundColor: "#f5f5f5",
                  backdropFilter: 'blur(10px)',
                  width: 400,
                  mx: 'auto', // margin left & right
                  my: 4, // margin top & bottom
                  py: 3, // padding top & bottom
                  px: 2, // padding left & right
                  display: 'flex',
                  flexDirection: 'column',
                  gap: 2,
                  borderRadius: 'sm',
                  boxShadow: 'md',
                }}
                variant="outlined"
              >
                <h1 className='center'>User</h1>
            <br/>
            <br/>
            <form onSubmit={handleSubmit}>
            <div>
            
              <Input
                name="name"
                placeholder="Name"
                value={state.name}
                onChange={nameHandler}
                sx={{
                  width: '300px',
                  margin: '0 auto',
                  backgroundColor: 'transparent',
                  
                  border: '1px solid black',
                  borderRadius: '4px',
                  '&:focus': {
                  borderColor: '#86b7fe',
                  boxShadow: '0 0 0 0.1rem rgba(13,110,253,.25)',
                  },
                }}
              />
              <div>{state.nameError}</div>
              <br/>
              <br/>
            </div>
    
            
    
            <div>
              <Input
                name="email"
                
                placeholder="Email Address"
                value={state.email}
                onChange={emailHandler}
                sx={{
                  width: '300px',
                  margin: '0 auto',
                  backgroundColor: 'transparent',
                  
                  border: '1px solid black',
                  borderRadius: '4px',
                  '&:focus': {
                  borderColor: '#86b7fe',
                  boxShadow: '0 0 0 0.1rem rgba(13,110,253,.25)',
                  },
                }}
              />
              <div>{state.emailError}</div>
              <br/>
              <br/>
            </div>
    
            <div>
              
              <Input
                name="password"
                type={showPass ? "text" : "password"} 
                placeholder="Password"
                value={state.password} 
                
                onChange={passHandler}
                sx={{
                  width: '300px',
                  margin: '0 auto',
                  backgroundColor: 'transparent',
                  border: '1px solid black',
                  borderRadius: '4px',
                  '&:focus': {
                  borderColor: '#86b7fe',
                  boxShadow: '0 0 0 0.1rem rgba(13,110,253,.25)',
                  },
                }}
              />
              <div>{state.passwordError}</div>
            
            </div>
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <input 
                  type="checkbox"
                  placeholder="Show Password" 
                  checked={showPass} 
                  onChange={() => setShowPass(prev => !prev)}
              />
              <br/>
              <br/>
            </div>
            <br/>
              
            <div style={{ textAlign: 'center' }}>
              <select
                name="role"
                value={state.role}
                onChange={roleHandler}
                className="select"
              >
                <option value="">Select Role</option>
                <option value="ADMIN">ADMIN</option>
                <option value="TEAMLEAD">TEAMLEAD</option>
                <option value="INTERN">INTERN</option>
              </select>
              <div>{state.roleError}</div>
              <br/>
              <br/>
            </div>
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <Button type="submit" style={{ marginRight: '8px', backgroundColor: '#2962ff', color: 'white' }}>Add</Button> 
            </div>
          </form>
    
              </Sheet>
          </main>      
            
                    
                <h1>User Details</h1>
                <hr />
                <br/>
                <table className="table-wrapper">
                    
                    <tbody>
                        <tr style={{ backgroundColor: '#bbdefb' }}>
                            <th className="table-header" style={{ width: '200px' }}>ID</th>
                            <th className="table-header" style={{ width: '200px' }}>Name</th>
                            <th className="table-header" style={{ width: '200px' }}>Email</th>
                            <th className="table-header" style={{ width: '200px' }}>Password</th>
                            <th className="table-header" style={{ width: '200px' }}>Role</th>
                            
                        </tr>
                        
                        {userList.map((post) => {
                            return (
                                <tr key={post._id} style={{ textAlign: 'left' }}>
                                    <td>{post._id}</td>
                                    <td>{post.name}</td>
                                    <td>{post.email}</td>
                                    <td>{post.password}</td>
                                    <td>{post.role}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                    
                </table>
                
            </div>
                  
        )
    }
    

export default User;
