import React, { useState } from 'react';
import { IState, iTodo } from './type';
import './style.css';
import ToDoDisplay from './components/tododisplay';
import TodoForm from './components/todoform';
import { useNavigate } from 'react-router-dom';

import Sheet from '@mui/joy/Sheet';
import CssBaseline from '@mui/joy/CssBaseline';



function Home() {
    
    const [state, setState] = useState<IState>({
        temp_input: "",
        temp_message: "",
        input_list: [],
        showTable: false,
        editItem: {
            id: '',
            input: '',
            message: '',
        },
    });

    const inputHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {value} = event.target;
        setState(prevState => ({ ...prevState, temp_input: value }));
    };

    const messageHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {value} = event.target;
        setState(prevState => ({ ...prevState, temp_message: value })); 
    };

    const btnAddHandler = () => {
        const { temp_input, temp_message, input_list } = state;
        if (temp_input.trim() === "") {
            alert("Please enter a text before adding.");
            return;
        }
        const newInputList = [
            ...input_list,
            {
                id: `${new Date().getTime() / 1000}`,
                input: temp_input,
                message: temp_message,
            }
        ];
        setState(prevState => ({
            ...prevState,
            input_list: newInputList,
            temp_input: "",
            temp_message: "",
        }));
    };

    const handleCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { checked } = event.target;
        setState(prevState => ({ ...prevState, showTable: checked }));
    };

    const handleDelete = (id: string) => {
        setState(prevState => ({
            ...prevState,
            input_list: prevState.input_list.filter(item => item.id !== id)
        }));
    };

    const handleEditButton = (item: iTodo) => {
        setState(state => ({ ...state, editItem: item  }));
    };

    const handleEditOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        if (name === "editInput") {
          setState((state) => ({...state, editItem: { ...state.editItem, input: value }}));
        } else if (name === "editMessage") {
          setState((state) => ({ ...state, editItem: { ...state.editItem, message: value }}));
        }
    };

    const handleUpdateButton = () => {
        const { editItem, input_list } = state;

        const newList = input_list.map(item => {
            if(item.id === editItem.id) {
              return editItem
            }
            else return item;
          })
          setState(state =>({ ...state, input_list: newList, editItem: {id: '', input: '', message: ''}}))
    };
      

    const handleCancel = () => {
        setState({...state, editItem: { id: '', input: '', message: '' } });
    };

    const navigate = useNavigate();

    const handleButtonLogout = () => {
        localStorage.removeItem('Token');
        navigate("/Login")
    };

    const handleButtonUser = () => {
        navigate("/User");
    };

    const handleButtonPost = () => {
        navigate("/Post")  
    };



    return (
        <div>
            <CssBaseline>
            <Sheet
        sx={{
          backgroundColor: '#f5f5f5',
          width: 300,
          mx: 'auto', // margin left & right
          my: 4, // margin top & bottom
          py: 3, // padding top & bottom
          px: 2, // padding left & right
          display: 'flex',
          flexDirection: 'column',
          gap: 2,
          borderRadius: 'sm',
          boxShadow: 'md',
        }}
        variant="outlined"
      >
        <h1 className='center'>Home</h1>
        <br/>
        <br/>

            <div>
                <button className='btn' onClick={handleButtonUser}>User</button>
                <button className='btn' onClick={handleButtonPost}>Post</button>
                <button className='btn' onClick={handleButtonLogout}>Logout</button>
            </div>
            
            <div className="basic-form-container">
                <TodoForm
                    btnAddHandler={btnAddHandler}
                    inputHandler={inputHandler}
                    messageHandler={messageHandler}
                    temp_input={state.temp_input}
                    temp_message={state.temp_message}
                />
            </div>
            </Sheet>
            <input type="checkbox" onChange={handleCheckboxChange} />{" "}
            <span>Show Table</span>
            <ToDoDisplay
                onEdit={handleEditButton}
                onDelete={handleDelete}
                showTable={state.showTable}
                input_list={state.input_list}
                handleEditOnChange={handleEditOnChange}
                handleUpdateButton={handleUpdateButton}
                handleCancel={handleCancel}
                editItem={state.editItem}
            />
        </CssBaseline>
        </div>
    );

}

export default Home;
