export interface IProp {}

export interface IState {
    postList: IPost[]
}

interface IPost{
    _id: string;
    userId: string;
    title: string;
    message: string;
    status: 'ACTIVE' | 'INACTIVE'
}

export interface IStateNewForm {
    id: string;
    title: string;
    message: string
}

// interface ITOdo {
//     _id: string;
//     input: string;
//     message: string
// }