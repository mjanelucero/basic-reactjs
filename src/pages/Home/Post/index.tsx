import { useState, useEffect } from "react";
import axios from "axios";
import { IProp, IState } from "./type";
import './style.css';
import { useNavigate } from 'react-router-dom';

import Input from '@mui/joy/Input';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { Button, MenuItem, Select } from '@mui/material';
import Sheet from '@mui/joy/Sheet';
import CssBaseline from '@mui/joy/CssBaseline';

function Posts(_props: IProp) {
    const [isEditMode, setIsEditMode] = useState<boolean>(false);
    const [isCancelMode, setIsCancelMode] = useState<boolean>(false);
    const [userList, setUserList] = useState<IState['postList']>([]);
    const [filterStatus, setFilterStatus] = useState('ACTIVE');

    const [state, setState] = useState({
        userId: "",
        _id: "",
        title: "",
        message: "",
        titleError: "",
        messageError: "",
    });

    useEffect(() => {
        handleRequestPost();
    }, []);

    const handleRequestPost = async () => {
        try {
            const response = await axios.get(
                "http://localhost:5173/api/post"
            );
            setUserList(response.data);
        } catch (error) {
            alert('ERROR');
        }
    };

    const titleHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setState({ ...state, title: value });
    };

    const messageHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setState({ ...state, message: value });
    };

    const errorValidate = () => {
        let titleError = "";
        let messageError = "";

        if (!state.title) {
            titleError = "Please input title";
        }

        if (!state.message) {
            messageError = "Message cannot be blank";
        }

        setState({
            ...state,
            titleError,
            messageError,
        });

        return !(titleError || messageError);
    };

    const handleDeletePost = async (post : any) => {
        try {
            let confirmDelete = "Are you sure you want to delete this post?";
            if (confirm(confirmDelete)) {
                await axios.delete(`http://localhost:5173/api/post/${post._id}`);
                handleRequestPost();
                alert(`Successfully Deleted the ${post._id}`);
            }
        } catch (error) {
            alert(error);
        }
    };

    const handleSubmit = async (e : React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const userID = Date.now().toString();
        const isValid = errorValidate();

        if (isValid) {
            const newState = { ...state, userId: userID };
            try {
                if (isEditMode) {
                    await axios.put(`http://localhost:5173/api/post/${state._id}`, state);
                    alert(`UPDATED successfully ID ${state._id}`);
                } else {
                    const response = await axios.post("http://localhost:5173/api/post", newState);
                    alert("Submitted");
                    setUserList(prevState => [...prevState, response.data]);
                }
                handleRequestPost();
            } catch (err) {
                console.log('Error:', err);
            }

            setState({
                userId: "",
                _id: "",
                title: "",
                message: "",
                titleError: "",
                messageError: "",
            });
            setIsEditMode(false);
            setIsCancelMode(false);
        }
    };

    const cancelHandler = () => {
        setIsEditMode(false);
        setIsCancelMode(false);

        setState({
            userId: "",
            _id: "",
            title: "",
            message: "",
            titleError: "",
            messageError: "",
        });
    };

    const handleEditClick = (post: any) => {
        setIsEditMode(true);
        setIsCancelMode(true);

        setState({
            userId: post.userId,
            _id: post._id,
            title: post.title,
            message: post.message,
            titleError: "",
            messageError: "",
        });
    };

    const handleChange = (event: any) => {
        setFilterStatus(event.target.value);
    };

    const navigate = useNavigate();

    const handleButtonHome = () => {
        navigate("/");
    };

    const handleButtonLogout = () => {
        localStorage.removeItem('Token');
        navigate("/Login")
    };

    const handleButtonUser = () => {
        navigate("/User")
    };

    return (
        
        <div className='center'>
            <div>
            <button className='btn' onClick={handleButtonHome}>Home</button>
            <button className='btn' onClick={handleButtonUser}>User</button>
            <button className='btn' onClick={handleButtonLogout}>Logout</button>
            </div>
            <main>
                <CssBaseline />
                <Sheet
                    sx={{
                        backgroundColor: '#f5f5f5',
                        backdropFilter: 'blur(10px)',
                        width: 400,
                        mx: 'auto', // margin left & right
                        my: 4, // margin top & bottom
                        py: 3, // padding top & bottom
                        px: 2, // padding left & right
                        display: 'flex',
                        flexDirection: 'column',
                        gap: 2,
                        borderRadius: 'sm',
                        boxShadow: 'md',
                    }}
                    variant="outlined"
                >
                    <h1 style={{ textAlign: 'center', alignItems: "center" }}>Post</h1>

                    <div>
                        {isEditMode && (
                            <Input
                                name="ID"
                                value={`ID: ${state._id}`}
                                readOnly
                                sx={{
                                    width: '300px',
                                    margin: '0 auto',
                                    backgroundColor: 'transparent',
                                    color: 'black',
                                    border: '1px solid black',
                                    borderRadius: '4px',
                                    '&:focus': {
                                        borderColor: '#86b7fe',
                                        boxShadow: '0 0 0 0.1rem rgba(13,110,253,.25)',
                                    },
                                }}
                            />
                        )}
                        <br />
                    </div>
                    <form onSubmit={handleSubmit}>
                        <Input
                            name="title"
                            placeholder="Title"
                            value={state.title}
                            onChange={titleHandler}
                            sx={{
                                width: '300px',
                                margin: '0 auto',
                                backgroundColor: 'transparent',
                                border: '1px solid black',
                                borderRadius: '4px',
                                '&:focus': {
                                    borderColor: '#86b7fe',
                                    boxShadow: '0 0 0 0.1rem rgba(13,110,253,.25)',
                                },
                            }}
                        />
                        <div>{state.titleError}</div>
                        <br /><br />
                        <Input
                            name="message"
                            placeholder="Message"
                            value={state.message}
                            onChange={messageHandler}
                            sx={{
                                width: '300px',
                                margin: '0 auto',
                                backgroundColor: 'transparent',
                                border: '1px solid black',
                                borderRadius: '4px',
                                '&:focus': {
                                    borderColor: '#86b7fe',
                                    boxShadow: '0 0 0 0.1rem rgba(13,110,253,.25)',
                                },
                            }}
                        />
                        <div>{state.messageError}</div>
                        <br /><br />

                        <div style={{ display: 'flex', justifyContent: 'center' }}>
                            <Button type="submit" style={{ marginRight: '8px', backgroundColor: '#2962ff', color: 'white' }}>{isEditMode ? 'Update' : 'Add'}</Button>
                            {isCancelMode && (
                                <Button type="button" onClick={cancelHandler} style={{ marginRight: '8px', backgroundColor: '#ff1744', color: 'white' }}>Cancel</Button>
                            )}
                        </div>
                    </form>
                    <br />
                </Sheet>
            </main>
            
            
            <h1>Post Details</h1>
            <hr />
            <br/>
            <table className="table-wrapper">
                <tbody>
                    <tr style={{ backgroundColor: '#bbdefb' }}>
                        <th style={{ width: '300px' }}>ID</th>
                        <th style={{ width: '200px' }}>Title</th>
                        <th style={{ width: '200px' }}>Message</th>
                        <th>
                            <Select
                                value={filterStatus}
                                onChange={handleChange}
                                variant="outlined"
                                style={{ fontWeight: 'bold' }}
                            >
                                <MenuItem value="ACTIVE">Active</MenuItem>
                                <MenuItem value="INACTIVE">Inactive</MenuItem>
                            </Select>
                        </th>
                        <th>Actions</th>
                    </tr>
                    {userList.filter((post) => post.status === filterStatus).map((post) => (
                        <tr key={post._id} style={{ textAlign: 'left' }}>
                            <td>{post._id}</td>
                            <td>{post.title}</td>
                            <td>{post.message}</td>
                            <td>{post.status}</td>
                            <td>
                                <Button
                                    style={{ backgroundColor: 'transparent', color: 'inherit' }}
                                    startIcon={<EditIcon />}
                                    onClick={() => handleEditClick(post)}
                                    disabled={isEditMode}
                                />
                                <Button
                                    style={{ backgroundColor: 'transparent', color: 'black' }}
                                    startIcon={<DeleteIcon />}
                                    onClick={() => handleDeletePost(post)}
                                    disabled={isEditMode}
                                />
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default Posts;