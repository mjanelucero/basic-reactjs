export interface IProp {}

export interface IState {
    temp_input: string;
    temp_message: string;
    input_list: iTodo[];
    showTable: boolean;
    editItem: iTodo;
}

export interface iTodo {
    id: string;
    input: string;
    message: string;
}

export interface iTodoDisplayProp {
    input_list: iTodo[];
    showTable: boolean;
    onDelete: (value: any) => void;
    onEdit: (value: any) => void;
    handleEditOnChange: (event: any) => void;
    handleUpdateButton: (event: any) => void;
    handleCancel: () => void
    editItem?: iTodo
}

export interface ITodoFormProp {
    inputHandler: (value: any) => void;
    messageHandler: (value: any) => void;
    btnAddHandler: () => void;
    temp_input: string;
    temp_message: string;
}

