import React, { useState } from "react";
import { IProp } from "./type";
import { Link, useNavigate } from 'react-router-dom';
import { accounts } from "../../../utils/authenticateAccount";

import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Face2Icon from '@mui/icons-material/Face2';
import Typography from '@mui/material/Typography';

function Login(props: IProp) {
    const [showPass, setShowPass] = useState<boolean>(false);
    const [username, setUsername] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const navigate = useNavigate();

    const handleInputUser = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setUsername(value);
    }

    const handleInputPass = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setPassword(value);
    }

    const handleLoginButton = () => {
        const token = {
            username,
            password
        };

        const authenticated = accounts
            .map((item) => JSON.stringify(item))
            .includes(JSON.stringify(token));
        if (authenticated) {
            localStorage.setItem("Token", JSON.stringify(token));
            navigate("/");
        } else {
            alert("Invalid account");
        }
    }


    return (
        <div>
            <CssBaseline />
            <Grid container justifyContent="center">
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                    <Box
                        sx={{
                            my: 8,
                            mx: 4,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <Avatar sx={{ md: 1, bgcolor: '#9e9e9e' }}>
                            <Face2Icon />
                        </Avatar>
                        <br/>
                        <Typography component="h1" variant="h5">Login</Typography>
                        <Box component="form" noValidate onSubmit={handleLoginButton} sx={{ mt: 1 }}>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                id="username"
                                label="Username"
                                name="username"
                                autoComplete="username"
                                autoFocus
                                value={username}
                                onChange={handleInputUser}
                            />
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type={showPass ? "text" : "password"}
                                id="password"
                                autoComplete="current-password"
                                value={password}
                                onChange={handleInputPass}
                            />
                            <FormControlLabel
                                control={<Checkbox checked={showPass} onChange={() => setShowPass(prev => !prev)} />}
                                label="Show Password"
                            />
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                sx={{ mt: 3, mb: 2 }}
                            >
                                Login
                            </Button>
                            <Grid container justifyContent="center">
                                <Grid item>
                                    <Link to="http://localhost:5173/Register">
                                        <h6>"Don't have an account? Sign Up Here"</h6>
                                    </Link>
                                </Grid>
                            </Grid>
                        </Box>
                    </Box>
                </Grid>
            </Grid>
        </div>
    );
}

export default Login;
