import { ITodoFormProp } from "../type";
import Sheet from '@mui/joy/Sheet';
import CssBaseline from '@mui/joy/CssBaseline';
import Input from '@mui/joy/Input';
import { Button } from '@mui/material';
import { Textarea } from "@mui/joy";

function TodoForm(props: ITodoFormProp) {
  const { inputHandler, messageHandler, btnAddHandler, temp_input, temp_message } = props;

  return (
    <div>
      <CssBaseline />
      {/* <form onSubmit={btnAddHandler}> */}
        <Sheet
          sx={{
            backgroundColor: "#f5f5f5",
            backdropFilter: 'blur(10px)',
            // width: 400,
            mx: 'auto', // margin left & right
            my: 4, // margin top & bottom
            py: 3, // padding top & bottom
            px: 2, // padding left & right
            display: 'flex',
            flexDirection: 'column',
            gap: 2,
            borderRadius: 'sm',
            boxShadow: 'md',
          }}
          variant="outlined"
        >
          <Input
            name="Title"
            placeholder="Name"
            type="text"
            onChange={inputHandler}
            value={temp_input}
            sx={{
              width: '200px',
              margin: '0 auto',
              backgroundColor: 'transparent',
              border: '1px solid black',
              borderRadius: '4px',
              '&:focus': {
                borderColor: '#86b7fe',
                boxShadow: '0 0 0 0.1rem rgba(13,110,253,.25)',
              },
            }}
          />
          <Textarea
            name="message"
            onChange={messageHandler}
            value={temp_message}
            placeholder="Enter Message"
            sx={{
              width: '200px',
              margin: '0 auto',
              backgroundColor: 'transparent',
              border: '1px solid black',
              borderRadius: '4px',
              '&:focus': {
                borderColor: '#86b7fe',
                boxShadow: '0 0 0 0.1rem rgba(13,110,253,.25)',
              },
            }}
          />
          <br/>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <Button type="submit" onClick={btnAddHandler} style={{ marginRight: '8px', backgroundColor: '#2962ff', color: 'white' }}>Add</Button>
          </div>
        </Sheet>
      {/* </form> */}
    </div>
  );
}

export default TodoForm;
