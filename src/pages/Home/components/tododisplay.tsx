import React from 'react';
import "../style.css";
import { iTodoDisplayProp } from "../type";

function ToDoDisplay(props: iTodoDisplayProp) {
    const { input_list, showTable, editItem, onDelete, onEdit, handleEditOnChange, handleUpdateButton, handleCancel } = props;

    return (
        <>
            {!showTable ? (
                <></>
            ) : (
                <table border={2}>
                    <thead>
                        <tr style={{ backgroundColor: '#bbdefb' }}>
                            <th style={{ width: '200px' }}>Text</th>
                            <th style={{ width: '200px' }}>Message</th>
                            <th style={{ width: '200px' }}>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {input_list.map((item, index) => {
                            if (item.id === editItem?.id && item.id) {
                                return (
                                    <tr key={item.id + index}>
                                        <td>
                                            <input name="editInput" type="text" value={editItem.input} onChange={handleEditOnChange} />
                                        </td>
                                        <td>
                                            <input name="editMessage" type="text" value={editItem.message} onChange={handleEditOnChange} />
                                        </td>
                                        <td>
                                            <button onClick={handleUpdateButton}>Update</button>
                                            <button onClick={handleCancel}>Cancel</button>
                                        </td>
                                    </tr>
                                );
                            } else {
                                return (
                                    <tr key={item.id + index}>
                                        <td>
                                            <span>{item.input}</span>
                                        </td>
                                        <td>{item.message}</td>
                                        <td>
                                            <button className="btn btn-primary" onClick={() => onEdit(item)}>Edit</button>
                                            <button className="btn btn-error" onClick={() => onDelete(item.id)}>Delete</button>
                                        </td>
                                    </tr>
                                );
                            }
                        })}
                    </tbody>
                </table>
            )}
        </>
    );
}

export default ToDoDisplay;
